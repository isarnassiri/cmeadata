###Complete input data for CMEA package###

###Description of data###
We demonstrate the application of the CMEA package by using the transcriptomic and single cell morphological profiles of human bone osteosarcoma cells (U-2 OS cells) in response to the 9515 drugs or small molecule compounds (1). The raw data include the transcripotimic profiles of 20340 drugs and small compound molecules, and single cell morpohological profiles of 19864 drugs and small compound molecules. These data sets were published in 2014  and are available as supplementary files of (2). We used a processed version of this data, which is available as supplementary of (3). The intersection of transcriptomic and single-cell morphological profiles includes 9515 drugs and small compound molecules, and use as a backend repository of CMEA package. 
The transcriptomic profile in backend repository is 9515*978 matric, including the fold change of the 978 landmark gene expression in response to the treatment of U-2 OS cells with 9515 drugs and small compound molecules (4). 

###Example of application###

You can use the following sample code to use the transcriptomic profile of "BRD-K37798499" as input of CMEA package (https://github.com/isarnassiri/CMEA):

setwd("... /data")

load("Cell_Morphology_Profile.rda")

load("Transcriptomic_Profile.rda") 

Cell_Morphology_Profile <- Cell_Morphology_Profile_Complete

Transcriptomic_Profile <- Transcriptomic_Profile_Complete

Cell_Morphology_Profile <- Cell_Morphology_Profile[-which(rownames(Cell_Morphology_Profile) %in% "BRD-K37798499"),]

Transcriptomic_Profile <- Transcriptomic_Profile[-which(rownames(Transcriptomic_Profile) %in% "BRD-K37798499"),]

library(CMEA)

Mapping()

###References###

1. Lamb, J., Crawford, E.D., Peck, D., Modell, J.W., Blat, I.C., Wrobel, M.J., Lerner, J., Brunet, J.P., Subramanian, A., Ross, K.N. et al. (2006) The connectivity map: Using gene-expression signatures to connect small molecules, genes, and disease. Science, 313, 1929-1935.

2. Wawer, M.J., Jaramillo, D.E., Dancik, V., Fass, D.M., Haggarty, S.J.,
Shamji, A.F., Wagner, B.K., Schreiber, S.L. and Clemons, P.A. (2014)
Automated Structure-Activity Relationship Mining: Connecting Chemical
Structure to Biological Profiles. Journal of Biomolecular Screening, 19,
738-748.

3. Wang, Z., Clark, N.R. and Ma'ayan, A. (2016) Drug-induced adverse events prediction with the LINCS L1000 data. Bioinformatics, 32, 2338-2345.

4. https://www.broadinstitute.org/chembio-therapeutics/mlpcnprofiling.